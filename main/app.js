function distance(first, second){
	nr=0;

		if (Array.isArray(first) &&  Array.isArray(second))
		{
			st = [...new Set(first)];
			nd = [...new Set(second)];
			for (var i=0; i< st.length; i++)
			{
				for(var j=0;j<nd.length;j++)
				{
					if(st[i] !== nd[j] && typeof(nd[j]) == typeof(st[i])) nr++;
					if(st[i] !== nd[j] && typeof(nd[j]) != typeof(st[i])) nr=nr+2;
				}
			}
		}
		else
		{
			throw new TypeError('InvalidType');
		}

	return nr;
}


module.exports.distance = distance